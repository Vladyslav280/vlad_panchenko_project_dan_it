const menu = $('.menu-icon');
const closeMenu = $('.close-menu-icon');
const navMobile = $('nav > .nav-mobile');
menu.on('click', () => {
    menu.removeClass('menu-icon--active');
    closeMenu.addClass('close-menu-icon--active');
    navMobile.addClass('nav-mobile--active');
});
closeMenu.on('click', () => {
    closeMenu.removeClass('close-menu-icon--active');
    navMobile.removeClass('nav-mobile--active');
    menu.addClass('menu-icon--active');
});



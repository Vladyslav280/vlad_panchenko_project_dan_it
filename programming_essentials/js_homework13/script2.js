// Реализовать возможность смены цветовой темы сайта пользователем.
// Технические требования:
//     Взять любое готовое домашнее задание по HTML/CSS.
//     Добавить на макете кнопку "Сменить тему".
//     При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
//     При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
//     Выбранная тема должна сохраняться и после перезагрузки страницы

let images = document.querySelectorAll('.image-to-show'); // pictures
let currentImage = 0; // index of image
let slideInterval = setInterval(nextSlide, 3000); // Interval of slide
let buttonStop = document.getElementById('btn-stop');
let buttonContinue = document.getElementById('btn-continue');
let buttonChangeTheme = document.getElementById('btn-change-theme');
let color = 'white';
function nextSlide() {
    images[currentImage].className = 'image-to-show';
    currentImage = (currentImage + 1) % images.length;
    images[currentImage].className = 'image-to-show showing';
}
buttonStop.onclick = function () {
    if (slideInterval) {
        clearInterval(slideInterval);
        slideInterval = null;
    } else if (!slideInterval) return false;
}
buttonContinue.onclick = function () {
    if (slideInterval) return false;
    else if (!slideInterval) slideInterval = setInterval(nextSlide, 3000);
}
buttonChangeTheme.onclick = function () {
    if (color === 'white') {
        color = 'yellow';
        document.body.style.background = color;
        localStorage.setItem('theme', color);
    } else if (color === 'yellow') {
        color = 'white';
        document.body.style.background = color;
        localStorage.setItem('theme', color);
    }
}
window.onload = function () {
    document.body.style.background = localStorage.getItem('theme');
}
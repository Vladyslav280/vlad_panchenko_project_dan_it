// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Экранирование - это техническое решение, которое позволяет менять управляющие символы на текст, перед такими
// символами мы ставим слэш \. На практике, удобно в использовании: кавычек в кавычках("\""), переход на новую строку("\n"),
// юникод("\u00A9"), слэш в кавычках("\\"), работа с путями ("\"C:\\Program Files\\"")

// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
//     соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


const createNewUser = () => {
    let currentDate = new Date();
    let selectFirstName = prompt("Enter your name");
    let selectLastName = prompt("Enter your last name");
    let selectBirthdayDate = prompt("Enter your birthday date (text in format dd.mm.yyyy)");
    selectBirthdayDate = `${selectBirthdayDate.substr(3, 2)}.${selectBirthdayDate.substr(0,2)}.${selectBirthdayDate.substr(6,4)}`
    let convertBirthdayDate = new Date(selectBirthdayDate);
    let user = {
        firstName: selectFirstName,
        lastName: selectLastName,
        birthday: convertBirthdayDate,
        getLogin() {
            let firstCharOfName = user.firstName.substr(0, 1);
            return `User login: ${firstCharOfName.toLowerCase() + user.lastName.toLowerCase()} `;
        },
        getAge() {
            let currentYear = currentDate.getFullYear();
            let birthdayYear = convertBirthdayDate.getFullYear();
            let currentMonth = currentDate.getMonth();
            let birthdayMonth = convertBirthdayDate.getMonth();
            birthdayMonth++;
            let currentDay = currentDate.getDate();
            let birthdayDay = convertBirthdayDate.getDate();
            let age = currentYear - birthdayYear;
            if ( currentMonth < (birthdayMonth - 1))
            {
                age--;
            }
            if (((birthdayMonth - 1) === currentMonth) && (currentDay < birthdayDay))
            {
                age--;
            }
            return `User age: ${age} `;
        },
        getPassword() {
            let firstCharOfName = user.firstName.substr(0, 1);
            return `User password: ${firstCharOfName.toUpperCase() + user.lastName.toLowerCase() + convertBirthdayDate.getFullYear()} `;
        }
    }
    Object.defineProperty(user, 'firstName', {
        writable: false,
        value: selectFirstName
    });
    Object.defineProperty(user, 'lastName', {
        writable: false,
        value: selectLastName
    });
    console.log(user, user.getLogin(), user.getAge(), user.getPassword());
    return console.log(user, user.getLogin(), user.getAge(), user.getPassword());;
}
createNewUser();
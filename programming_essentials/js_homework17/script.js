// Создать объект студент "студент" и проанализировать его табель.
// Технические требования:
//     Создать пустой объект student, с полями name и lastName.
//     Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
//     В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel
//     при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
//     Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент
//     переведен на следующий курс.
//     Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.
let mark, subject;
let subjectsCount = 0;
let badMarksCount = 0;
let sumOfMarks;
let marksArray;
let arithmeticalMean;

let student = {
    name: prompt('Введите имя студента'),
    lastName: prompt('Введите фамилию студента'),
    tabel: []
}

if (!student.name) {
    do student.name = prompt('Введите имя студента');
    while (!student.name);
}
if (!student.lastName) {
    do student.lastName = prompt('Введите фамилию студента');
    while (!student.lastName);
}

do {
    subject = prompt('Введите предмет');
    if (subject) mark = +prompt('Оценка');
    if (subject && mark && subject !== ' ') {
        if (mark < 4) badMarksCount++;
        student.tabel[subjectsCount] = `${subject}: ${mark}`;
        subjectsCount++;
    }
} while (subject);
if (badMarksCount === 0) alert(' Студент переведен на следующий курс.');

marksArray = student.tabel.map(item => {
     if (!parseInt(item.charAt(item.length - 2))) return parseInt(item.charAt(item.length - 1));
     if (parseInt(item.charAt(item.length - 2))) return parseInt(item.slice(-2));
})
sumOfMarks = marksArray.reduce(function (a,b) {
    return a + b;
})
arithmeticalMean = sumOfMarks / marksArray.length;
if (arithmeticalMean > 7) alert('Студенту назначена стипендия.');


console.log('last mark: '+mark);
console.log('last subject: '+subject);
console.log('subject count: '+subjectsCount);
console.log('bad marks count: '+badMarksCount);
console.log(student.tabel);
console.log('mark array: '+marksArray);
console.log('Sum of marks:' + sumOfMarks)
console.log('arithmetical mean: ' + arithmeticalMean)






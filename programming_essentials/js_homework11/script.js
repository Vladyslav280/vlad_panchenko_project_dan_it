/**
 *
      Теоретический вопрос
 Почему для работы с input не рекомендуется использовать события клавиатуры?

 Для работы с инпутами не рекомендуется использовать события клавиатуры, так как, если нужно ввести нужный символ
 на клавише, а на эту клавишу повешан слушатель, то во время ввода будут вызывать неожиданные события

      Задание
 Реализовать функцию подсветки нажимаемых клавиш.

      Технические требования:
 В файле index.html лежит разметка для кнопок.
 Каждая кнопка содержит в себе название клавиши на клавиатуре
 По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет.
 При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной.
 Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S,
 и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.
 *
 */

let buttons = document.querySelectorAll('.btn');
console.log(buttons);
document.addEventListener('keydown', (event) => {
    for (let item of buttons) {
        item.style.background = 'black';
        if (event.code === `Key${item.innerText}` || event.code === item.innerText) item.style.background = 'blue';
    }
})
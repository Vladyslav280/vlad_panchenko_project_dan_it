/**
 * Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.
 * Технические требования:
 *    Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут
 *    три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами),
 *    n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему
 *    правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
 *    Считать с помощью модального окна браузера число, которое введет пользователь (n).
 *    С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
 *    Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
 */


function fib(f0, f1, n) {
    if (n === 0) {
        if (f1 === 0 || f0 === 0) return fib(f0, f1, n + 1) - fib(f0, f1 , n + 2);
        else return f0;
    }
    else if (n >= 0) return fib(f1, f0 + f1, n - 1);
    else if (n < 0) return fib(f0, f1 , n + 2) - fib(f0, f1, n + 1);
}
let indexNum;
do {
    indexNum = +prompt('Please, enter index of number.');
} while (isNaN(indexNum));
console.log(fib(0, 1, indexNum));
alert(fib(0, 1, indexNum));





























// const fib = function (n) {
//     if (n < 0) return fib(n + 2) - fib(n + 1);
//     else if (n === 0) return 0;
//     else if (n === 1) return 1;
//     else if (n > 0) return fib(n - 2) + fib(n - 1);
// }
// let indexNum;
//     do {
//         indexNum = +prompt('Please, enter index of number.');
//     } while (isNaN(indexNum));
// console.log(fib(indexNum));
// alert(fib(indexNum));


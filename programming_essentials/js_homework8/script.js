/**
 *      Теоретический вопрос
 Опишите своими словами, как Вы понимаете, что такое обработчик событий.

 // Обработчик событий - это функция, которая обрабатывает событие и определяет как на него реагировать.
 // В Обработчик мы указываем тип события, функцию-обработчик и опции(необязательно)

        Задание
 Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript,
 без использования фреймворков и сторонник библиотек (типа Jquery).

        Технические требования:
 При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
 Это поле будет служить для ввода числовых значений
        Поведение поля должно быть следующим:
 // При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
 Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
 Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
 Значение внутри поля ввода окрашивается в зеленый цвет.
 При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
 Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
 под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
 В папке img лежат примеры реализации поля ввода и создающегося span.
 */

const label = document.createElement('label')
const input = document.createElement('input');
const backgroundInput = document.createElement('div');
label.innerText = 'Price, $ '

label.append(input);
backgroundInput.append(label);
document.body.append(backgroundInput)

let closeButton;
let price;
let div;
let error;

const focusButton = () => {
    input.style.outline = "3px solid green";
    backgroundInput.style.background = '';

    if (error) {
        error.remove();
    }
}

const blurButton = () => {
    input.style.outline = "";

    if(input.value > 0) {
        div = document.createElement('div');
        price = document.createElement('span');
        closeButton = document.createElement('button');

        closeButton.innerText = 'X';
        price.innerText = `Текущая цена: ${input.value}$.`;

        div.append(closeButton);
        div.prepend(price);
        document.body.prepend(div);

        backgroundInput.style.background = 'green'

        closeButton.addEventListener('click', event => {
           if (event.target.tagName === 'BUTTON') {
               event.target.parentNode.remove();
               input.value = '';
               backgroundInput.style.background = 'red';
           }
        });

    } else if (input.value <= 0) {
        input.style.outline = "3px solid red";
        error = document.createElement('p');
        error.innerText = 'Please enter correct price';
        document.body.append(error);
    }

    input.value = ''; // <= Сделал очистку поля при каждом блюре для удобства
}
input.addEventListener('focus', focusButton);
input.addEventListener('blur', blurButton);






let scrollButton = document.createElement('a');

$(scrollButton).css('width', '60px');
$(scrollButton).css('height', '60px');
$(scrollButton).css('background', 'black');
$(scrollButton).css('position', 'fixed');
$(scrollButton).css('zIndex', '10');
$(scrollButton).css('bottom', '50px');
$(scrollButton).css('right', '50px');
$(scrollButton).css('text-decoration', 'none');
$(scrollButton).css('color', 'white');
$(scrollButton).css('text-align', 'center');
$(scrollButton).css('line-height', '60px');
$(scrollButton).css('display', 'none');
$(scrollButton).attr('href', "#top");
$(scrollButton).text('TOP');

$(scrollButton).appendTo('body');

$('.header__list-item-link').click(function () {
        let elementClick = $(this).attr("href")
        let destination = $(elementClick).offset().top;
        $('html, body').animate({
            scrollTop: destination
        }, 1000, 'swing');
});

$(document).scroll(function () {
    if (window.pageYOffset > 300) $(scrollButton).css('display', 'block');
    else if (window.pageYOffset < 300) $(scrollButton).css('display', 'none');
});

$(scrollButton).click(function () {
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 1000, 'swing');
});

$('#button').click(function () {
    $('.fvblock__wrapper').slideToggle();
})







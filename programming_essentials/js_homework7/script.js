// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//
// DOM - объектная модель документа, которая представляет собой структуру объектов и узлов, которые имеют свойста и методы
// С помощью JS, мы можем получать доступ, удалять, добавлять и изменять узлы DOM-a;
// Каждый элемент в документе - это части DOM-a, которые могут быть доступны и изменяться с помощью DOM, или скриптовым
// языком вроде JS
//
// Задание
//     Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.

// Технические требования:
//     Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент,
//     к которому будет прикреплен список (по дефолту должен быть document.body).
//     Каждый из элементов массива вывести на страницу в виде пункта списка;
//     Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его
//     на страницу;
//     Примеры массивов, которые можно выводить на экран:
//
//     ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//     ["1", "2", "3", "sea", "user", 23];
//     Можно взять любой другой массив.
// Необязательные задания продвинутой сложности:
//     Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив,
//     выводить его как вложенный список. Пример такого массива:
//     ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.
//     Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.

const arrayToString = function (array, parent = document.body) {
    // array = array.flat(Infinity);
    parent = document.createElement(parent);
    document.body.append(parent);
    let listItem;
    array.map(function unpackArray(item) {
        listItem = document.createElement('li');
        if (Array.isArray(item)) {
            item.map(unpackArray)
        } else {
            listItem.innerHTML = `${item}`;
            parent.append(listItem);
        }
    });

    let handler = () => {
        document.body.remove();
    }
    setTimeout(handler, 3000);

}
arrayToString(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], `ol`);
arrayToString(["1", "2", "3", "sea", "user", 23], `ul`);
arrayToString(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], `ul`);
arrayToString(["Kharkiv", ["Kiev", ["Borispol", "Irpin"], "Odessa"], ["Lviv", ["Dnieper"]]], `ul`);
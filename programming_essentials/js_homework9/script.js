/**
 *
 * Реализовать переключение вкладок (табы) на чистом Javascript.

 Технические требования:
 В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку
 отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт.
 В комментариях указано, какой текст должен отображаться для какой вкладки.
 Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
 Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
 При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.
 *
 */

let tabs = document.querySelector('.tabs');
let akaliText = document.querySelector('#akali');
let anaviaText = document.querySelector('#anivia');
let dravenText = document.querySelector('#draven');
let garenText = document.querySelector('#garen');
let katerinaText = document.querySelector('#katerina');
const akali = document.querySelector('#akali-button');
const anavia = document.querySelector('#anivia-button');
const draven = document.querySelector('#draven-button');
const garen = document.querySelector('#garen-button');
const katerina = document.querySelector('#katerina-button');
let prevElement;
akaliText.classList.remove('content-item');
let handler = (event) => {
    akali.classList.remove('active');
    if (prevElement) prevElement.classList.remove('active');
    if (event.target === akali) akaliText.classList.remove('content-item');
    if (event.target !== akali) akaliText.classList.add('content-item');
    if (event.target === anavia) anaviaText.classList.remove('content-item');
    if (event.target !== anavia) anaviaText.classList.add('content-item');
    if (event.target === draven) dravenText.classList.remove('content-item');
    if (event.target !== draven) dravenText.classList.add('content-item');
    if (event.target === garen) garenText.classList.remove('content-item');
    if (event.target !== garen) garenText.classList.add('content-item');
    if (event.target === katerina) katerinaText.classList.remove('content-item');
    if (event.target !== katerina) katerinaText.classList.add('content-item');
    event.target.classList.add('active');
    prevElement = event.target;
}
tabs.addEventListener('click', handler);




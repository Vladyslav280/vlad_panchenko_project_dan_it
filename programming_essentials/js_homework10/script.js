/**
 *
 * Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript,
 * без использования фреймворков и сторонник библиотек (типа Jquery).

 Технические требования:
    В файле index.html лежит разметка для двух полей ввода пароля.
    По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
    иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна
    отображаться вместо текущей.
    Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
    Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
    По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
    Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
    Если значение не совпадают - вывести под вторым полем текст красного цвета Нужно ввести одинаковые значения
    После нажатия на кнопку страница не должна перезагружаться
    Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
 *
 */

const enterInput = document.getElementById('enter-password');
const confirmInput = document.getElementById('confirm-password');
const inputWrapper = document.getElementById('input-wrapper');
const enterShowIcon = document.getElementById('enter-eye');
const enterHideIcon = document.getElementById('enter-eye-slash');
const confirmShowIcon = document.getElementById('confirm-eye');
const confirmHideIcon = document.getElementById('confirm-eye-slash');
const warning = document.createElement('p');
const showCharsEnter = () => {
    if (enterInput.getAttribute('type') === 'password') {
        enterInput.setAttribute('type', 'text');
        enterShowIcon.style.display = 'none';
        enterHideIcon.style.display = 'inline';
    } else {
        enterInput.setAttribute('type', 'password');
        enterShowIcon.style.display = 'inline';
        enterHideIcon.style.display = 'none';
    }
}
const hideCharsConfirm = () => {
    if (confirmInput.getAttribute('type') === 'password') {
        confirmInput.setAttribute('type', 'text');
        confirmShowIcon.style.display = 'none';
        confirmHideIcon.style.display = 'inline';
    } else {
        confirmInput.setAttribute('type', 'password');
        confirmShowIcon.style.display = 'inline';
        confirmHideIcon.style.display = 'none';
    }
}
const comparePasswords = () => {
    warning.innerText = 'You must enter equal passwords';
    warning.style.color = 'red';
    if (enterInput.value === confirmInput.value) {
        enterInput.value = '';
        confirmInput.value = '';
        // Очищение полей - имитация отправки на сервер
        if (inputWrapper.childNodes.length > 7) inputWrapper.removeChild(inputWrapper.lastChild);
        alert('You are welcome!');
    } else if (enterInput.value !== confirmInput.value) {
        if (inputWrapper.childNodes.length === 7) inputWrapper.append(warning);
    }
}


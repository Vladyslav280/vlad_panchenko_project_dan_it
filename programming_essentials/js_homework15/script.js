// Теоретический вопрос
// Напишите как вы понимаете рекурсию. Для чего она используется на практике?
//
//  Рекурсия-это функция, которая вызывает сама себя. На практике используют обычно в функции, в которой есть
//  проверка: если условие не выполняется, то вызвать эту же функцию снова.
//
//     Задание
//     Реализовать функцию подсчета факториала числа.
//
// Технические требования:
//     Считать с помощью модального окна браузера число, которое введет пользователь.
//     С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
//     Использовать синтаксис ES6 для работы с перемеными и функциями.
//  Необязательное задание продвинутой сложности:
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел число,
//     либо при вводе указал не число, - спросить число заново
//     (при этом значением по умолчанию для него должна быть введенная ранее информация).

const getFactorial = function (n) {
    if (n !== 1) return n * getFactorial(n - 1);
    else return 1;
}
let userNum;
do {
    userNum = +prompt('Please, enter the number.');
} while (!userNum);
alert(getFactorial(userNum));
export function findObjectValues(obj, searchKey) {
    try {
        if (obj.hasOwnProperty(searchKey)) {
            for (let [key, value] of Object.entries(obj)) {
                if (searchKey === key) return value
            }
        }
        else throw new Error(`Oops! Property ${searchKey} not found`)
    } catch (e) {
        console.error(e);
    }
}
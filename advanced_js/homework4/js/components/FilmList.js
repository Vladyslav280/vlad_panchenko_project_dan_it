import {Ul} from "./Ul.js";
import {Film} from "./Film.js";
import {findObjectValues} from "../functions/findObjectValues.js";

export function FilmList(data) {
    this.data = data;
    this.filmList = new Ul(document.body);
    this.filmList.getUl().style.listStyleType = 'none';

    return this.data.map(filmItem => {
        this.filmItem = filmItem;
        this.film = new Film(this.filmList, findObjectValues(this.filmItem, 'episodeId'), findObjectValues(this.filmItem, 'name'), findObjectValues(this.filmItem, 'characters'), findObjectValues(this.filmItem, 'openingCrawl'));
    });
}
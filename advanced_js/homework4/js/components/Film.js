import {Li} from "./Li.js";
import {Ul} from "./Ul.js";
import {findObjectValues} from "../functions/findObjectValues.js";

export function Film(parentName, episodeId, name, characters, openingCrawl) {
    this.parentName = parentName;
    this.episodeId = episodeId;
    this.name = name;
    this.characters = characters;
    this.openingCrawl = openingCrawl;

    this.arraysOfPromises = this.characters.map(characterLink => {
        this.characterLink = characterLink;
        return fetch(this.characterLink)
            .then(characterResponse => {
                this.characterResponse = characterResponse;
                return this.characterResponse.json();
            })
            .then(characterData => {
                this.characterData = characterData;
                return findObjectValues(this.characterData, 'name') });
    });

    this.films = Promise.all(this.arraysOfPromises)
        .then(charactersArray => {
            this.film = new Li(this.parentName.getUl(), '');
            this.filmInfo = new Ul(this.film.getLi());
            this.filmEpisode = new Li(this.filmInfo.getUl(), this.episodeId);
            this.filmName = new Li(this.filmInfo.getUl(), this.name);
            this.filmCharacters = new Ul(this.filmInfo.getUl());
            this.filmCrawl = new Li(this.filmInfo.getUl(), this.openingCrawl);

            this.filmInfo.getUl().style.background = 'grey';
            this.filmInfo.getUl().style.margin = '20px 0';
            this.filmInfo.getUl().style.listStyleType = 'none';
            this.charactersArray = charactersArray;
            return this.charactersArray.map(characterName => {
                this.characterName = characterName;
                this.character = new Li(this.filmCharacters.getUl(), this.characterName)
            }); });

}
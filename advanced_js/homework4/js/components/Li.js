export function Li(parentName, value) {
    this.parentName = parentName;
    this.text = value;
    this.li = document.createElement('li');
    this.li.innerHTML = value;
    this.parentName.append(this.li);
}
Li.prototype.getLi = function () { return this.li }
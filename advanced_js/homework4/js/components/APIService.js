export function APIService(url) {
    this.url = url;
    return fetch(this.url)
        .then(response => {
            return response.json();
        })
        .then(result => {
            return result;
        });
}
/**
 * Теоретический вопрос
 * Обьясните своими словами, как вы понимаете асинхронность в Javascript
 *
 * Задание
 * Написать программу "Я тебя по айпи вычислю"
 *
 * Технические требования:
 * Создать простую HTML страницу с кнопкой Вычислить по IP.
 * По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить
 * оттуда IP адрес клиента.
 * Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
 * Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион,
 * город, район города.
 * Все запросы на сервер необходимо выполнить с помощью async await.
 */

/**
 * Асинхронность в JavaScript - это стиль программирования, при выполнении которого результат будет доступен
 * не сразу - лишь через время. Реализуется асинхронность через промисы, с помощью которых можно выстраивать
 * асинхронную цепочку действий. Они помещаются в очередь событий, не блокируя при этом код из основного потока,
 * затем они завершаться как можно скорее и вернут свои результаты в среду JS.
 */

window.onload = () => {
    const button = document.createElement('button');
    button.innerText = 'Вычислить по IP';
    document.body.append(button);

    button.addEventListener('click', async () => {
        const getIP = await fetch('https://api.ipify.org/?format=json');
        const response = await getIP.json();
        console.log(response.ip);

        const getDataFromIP = await fetch(`http://ip-api.com/json/${response.ip}?fields=continent,country,regionName,city,district`);
        const ip = await getDataFromIP.json();
        console.log(ip)

        const userInfo = document.createElement('ul');
        userInfo.style.listStyleType = 'none';

        for (let i = 0; i < Object.keys(ip).length; i++) {
            if (Object.keys(ip)[i].index === Object.values(ip)[i].index) {
                let info = document.createElement('li');

                if (Object.values(ip)[i] === '') info.innerText = `${Object.keys(ip)[i]}: none`;
                else info.innerText = `${Object.keys(ip)[i]}: ${Object.values(ip)[i]}`;

                userInfo.append(info);
            }

        }
        await document.body.append(userInfo);
        console.log(Object.keys(ip));
        console.log(Object.values(ip));




    })
}
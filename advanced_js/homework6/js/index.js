/**
 * Створити поле 10*10 за допомогою елемента <table>.
 *
 * Суть гри: будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір. Користувач повинен протягом відведеного часу встигнути клацнути на зафарбовану комірку. Якщо користувач встиг це зробити, вона забарвлюється зелений колір, користувач отримує 1 очко. Якщо не встиг – вона забарвлюється у червоний колір, комп'ютер отримує 1 очко.
 *
 * Гра триває доти, доки половина комірок на полі не будуть пофарбовані у зелений чи червоний колір. Як тільки це станеться, той гравець (людина чи комп'ютер), чиїх комірок на полі більше, перемагає.
 *
 * Гра повинна мати три рівні складності, що вибираються перед стартом гри:
 * Легкий – нова комірка підсвічується кожні 1.5 секунди;
 * Середній - нова комірка підсвічується раз на секунду;
 * Важкий - нова комірка підсвічується кожні півсекунди.
 *
 * Після закінчення гри вивести на екран повідомлення про те, хто переміг.
 *
 * Після закінчення гри має бути можливість змінити рівень складності та розпочати нову гру.
 *
 * Використовувати функціонал ООП під час написання програми.
 *
 * За бажанням, замість фарбування комірок кольором, можна вставляти туди картинки.
 */

import {Cell} from "./classes/Cell.js";
import {Component} from "./classes/Component.js";
import {Table} from "./classes/Table.js";
import {Modal} from "./classes/Modal.js";
import {Paragraph} from "./classes/Paragraph.js";
import {Input} from "./classes/Input.js";
import {Button} from "./classes/Button.js";
import {ModalEnterName} from "./classes/ModalEnterName.js";
import {ModalSelectLevel} from "./classes/ModalSelectLevel.js";
import {Game} from './classes/Game.js';

// const cell = new Cell({tagName: 'div', parentName: document.querySelector('.main'), classList: 'table__cell'});

// const modal = new Modal('form', document.body, 'modal');
// modal.build();

// const parag = new Paragraph('p', document.body, '', 'asdsadad');
// parag.build();
//
// const input = new Input('input', document.body, '', 'text', 'asdadadadad');
// input.build()

// const button = new Button('button', document.body, '', 'Send', 'click',
//     () => console.log('yeap'));
// button.build();

// const modalEnterName = new ModalEnterName('form', document.body, 'modal');
// modalEnterName.build()

// const modalSelectLevel = new ModalSelectLevel('form', document.body, 'modal');
// modalSelectLevel.build()

const game = new Game();
game.start();







export class Player {
    constructor(name) {
        this.name = name;
        this.points = 0;
        this.cells = [];
    }
    build() {
        this.p = document.createElement('p')
        this.p.innerText = `${this.name}: ${this.points}`;
        document.querySelector('.main').prepend(this.p);
    }
}
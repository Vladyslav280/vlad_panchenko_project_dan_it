export class Component {
    constructor(tagName, parent, classList) {
        this._tagName = tagName;
        this._parent = parent;
        this._classList = classList;
    }
    build() {
        this._component = document.createElement(`${this._tagName}`);
        this._component.classList = this._classList;
        this._parent.append(this._component);
    }
    get tagName() {
        return this._tagName;
    }
    get component() {
        return this._component;
    }
    get parent() {
        return this._parent
    }
    get classList() {
        return this._classList;
    }

}
import {Component} from "./Component.js";

export class Input extends Component {
    constructor(tagName, parent, classList, type) {
        super(tagName, parent, classList);
        this._type = type;
    }
    build() {
        super.build();
        this.component.setAttribute('type', this._type);
    }
    get value() {
        return this._component.value;
    }
    set value(val) {
        this._component.value = val;
    }
}
import {Component} from "./Component.js";
import {Cell} from './Cell.js'

export class Table extends Component {
    constructor(tagName, parent, classList, cols, rows) {
        super(tagName, parent, classList);
        this.cols = cols;
        this.rows = rows;
    }
    build() {
        super.build();

        this.culumnId = 0;

        for (let i = 0; i < this.rows; i++) {
            this.row = document.createElement('tr');
            this._component.append(this.row);
            for (let j = 0; j < this.cols; j++) {
                const column = new Cell('td', this.row, 'table__cell');
                column.build();
            }
        }
        document.querySelectorAll('.table__cell').forEach(item => {
            this.culumnId++;
            item.setAttribute('cell_id', this.culumnId);
        });
    }
}
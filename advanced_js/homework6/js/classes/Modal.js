import {Component} from "./Component.js";

export class Modal extends Component {
    constructor(tagName, parent, classList) {
        super(tagName, parent, classList);
    }
    build() {
        this._component = document.createElement(`${this.tagName}`);
        this._component.classList = `${this._classList}`;
        this._parent.prepend(this._component);
    }
}
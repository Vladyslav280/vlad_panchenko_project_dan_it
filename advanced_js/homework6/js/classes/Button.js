import {Component} from "./Component.js";

export class Button extends Component {
    constructor(tagName, parent, classList, text, eventName, listener) {
        super(tagName, parent, classList);
        this.text = text;
        this.eventName = eventName;
        this.listener = listener
    }
    build() {
        super.build();
        this._component.innerText = `${this.text}`
        this._component.addEventListener(this.eventName, this.listener)
    }
    get classList() {
        return this.component.classList;
    }
    set classList(value) {
        return this.component.classList = `${this._classList} ${value}`;
    }
}
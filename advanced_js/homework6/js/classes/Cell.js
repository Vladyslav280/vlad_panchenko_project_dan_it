import {Component} from "./Component.js";

export class Cell extends Component {
    constructor(tagName, parent, classList, attribute) {
        super(tagName, parent, classList);
        this.used = 0;
        this._attribute = attribute;
    }

    build() {
        super.build();

        this.component.addEventListener('click', () => {
            this.used = 1;
            console.log('yeap')
            console.log(this.used);
            this.checkCell();
        });
    }

    checkCell() {
        if (this.used === 0) this.component.classList = `${this.classList}`;
        else if (this.used === 1) {
            // +1
            this.component.classList = `${this.classList} cell--positive`
        } else if (this.used === 2) {
            // + 1 to comp
            this.component.classList = `${this.classList} cell--negative`;
        }
    }
    get attrs() {
        return this._component.getAttribute('cell_id');
    }
    set attrs(value) {
        this._component.setAttribute('cell_id', value);
    }
}

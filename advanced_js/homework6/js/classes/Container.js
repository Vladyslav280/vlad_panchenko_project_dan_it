import {Component} from './Component.js'

export class Container extends Component {
    constructor(tagName, className, parentName, flexDirection) {
        super(tagName, className, parentName);
        this.flexDirection = flexDirection;
    }
    build() {
        super.build();
        this.component.style.flexDirection = `${this.flexDirection}`;
    }
}
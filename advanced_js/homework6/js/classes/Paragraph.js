import {Component} from "./Component.js";

export class Paragraph extends Component {
    constructor(tagName, parent, classList, text) {
        super(tagName, parent, classList);
        this.text = text;
    }
    build() {
        super.build();
        this._component.innerText = `${this.text}`;
    }
}
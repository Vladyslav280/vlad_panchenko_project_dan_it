import {Modal} from './Modal.js'
import {Paragraph} from "./Paragraph.js";
import {Input} from "./Input.js";
import {Button} from "./Button.js";
import {ModalSelectLevel} from "./ModalSelectLevel.js";

export class ModalEnterName extends Modal {
    constructor(tagName, parent, classList) {
        super(tagName, parent, classList);
    }

    build() {
        super.build();
        this.parapraph = new Paragraph('p', this.component, 'modal__text', 'Hello, please enter your name');
        this.input = new Input('input', this.component, 'modal__input', 'text');
        this.button = new Button('button', this.component, 'modal__button', 'Next');

        this.component.setAttribute('modal', 'name');

        this.parapraph.build();
        this.input.build();
        this.button.build();

        this.component.addEventListener('submit', event => {
            event.preventDefault();
            try {
                if (this.input.value === '') {
                    throw new Error('Ошибка! Поле не заполнено');
                } else {
                    this.name = this.input.value;
                    this.component.remove();
                }
            } catch (err) {
                console.error(err);
            }
        });
    }
    get playerName() {
        return this.name;
    }
}

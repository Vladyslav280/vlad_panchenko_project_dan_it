import {Modal} from './Modal.js'
import {Paragraph} from "./Paragraph.js";
import {Container} from "./Container.js";
import {Button} from "./Button.js";
import {Table} from "./Table.js";

export class ModalSelectLevel extends Modal {
    constructor(tagName, parent, classList) {
        super(tagName, parent, classList);
    }
    build(name) {
        super.build();

        this.paragraph = new Paragraph('p', this.component, 'modal__text', 'Choose the level');
        this.paragraph.build();
        this.container = new Container('div', this.component, 'flex-center-container');
        this.container.build();
        this.buttonEasy = new Button('div', this.container.component, 'modal__option-button', 'Easy');
        this.buttonMedium = new Button('div', this.container.component, 'modal__option-button', 'Medium');
        this.buttonHard = new Button('div', this.container.component, 'modal__option-button', 'Hard');
        this.button = new Button('button', this.component, 'modal__button', 'Start');

        this.buttonEasy.build();
        this.buttonMedium.build();
        this.buttonHard.build();
        this.button.build();

        this.component.setAttribute('modal', 'level');

        this.container.component.addEventListener('click', event => {
            this.level = event.target.innerText;
            this.container.component.childNodes.forEach(item => item.classList.remove('selected'));
            event.target.classList.add('selected');
        });

        this.component.addEventListener('submit', event => {
            event.preventDefault();
            try {
                if (!this.level) throw new Error('Пожалуйста, выберите уровень!');
                else {
                    localStorage.setItem(`player`, JSON.stringify({ name: name, level: this.level }));
                    this.component.remove();
                }
            } catch (err) {
                console.error(err);
            }
        });
    }
}
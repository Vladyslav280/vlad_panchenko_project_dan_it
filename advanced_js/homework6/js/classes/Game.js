import {Table} from "./Table.js";
import {ModalEnterName} from "./ModalEnterName.js";
import {ModalSelectLevel} from "./ModalSelectLevel.js";
import {Player} from "./Player.js";

export class Game {
    constructor() {

    }

    start() {
        this.modalEnterName = new ModalEnterName('form', document.body, 'modal');
        this.modalEnterName.build();

        document.body.addEventListener('submit', async event => {
            if (event.target.getAttribute('modal') === 'name') {
                this.modalSelectLevel = new ModalSelectLevel('form', document.body, 'modal');
                this.modalSelectLevel.build(this.modalEnterName.playerName);
            }
            if (event.target.getAttribute('modal') === 'level') {
                this.table = new Table('table', document.querySelector('.main'), 'table', 10, 10);
                this.table.build();

                this.player = new Player(JSON.parse(localStorage.getItem('player')));
            }
        });
    }
}


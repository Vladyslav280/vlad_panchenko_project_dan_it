
/**
 * Задание 7
 * Дополните код так, чтоб он был рабочим
 */
const array = ['value', () => 'showValue'];

let [value, showValue] = array;

console.group('Seventh task');
console.log(value);
console.log(showValue());
console.groupEnd();

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'

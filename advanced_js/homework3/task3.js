
    /**
    * Задание 3
    * У нас есть объект user:
    *
    * const user1 = {
    *   name: "John",
    *   years: 30
    * };
    * Напишите деструктурирующее присваивание, которое:
    *
    * свойство name присвоит в переменную name
    * свойство years присвоит в переменную age
    * свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
    * Выведите переменные на экран.
    */
    const user1 = {
    name2: "John",
    years: 30
}
    let {name2, years, isAdmin = false} = user1;
    console.group('Third task');
    console.log(name2, years, isAdmin);
    console.groupEnd();

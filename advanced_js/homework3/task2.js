
    /**
    * Задание 2
    * Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
    *
    * Создайте на его основе массив charactersShortInfo, состоящий из объектов,
    * в которых есть только 3 поля - name, lastName и age.
    */
    const characters = [
    {
        name1: "Елена",
        lastName1: "Гилберт",
        age1: 17,
        gender: "woman",
        status: "human"
    },
    {
        name1: "Кэролайн",
        lastName1: "Форбс",
        age1: 17,
        gender: "woman",
        status: "human"
    },
    {
        name1: "Аларик",
        lastName1: "Зальцман",
        age1: 31,
        gender: "man",
        status: "human"
    },
    {
        name1: "Дэймон",
        lastName1: "Сальваторе",
        age1: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name1: "Ребекка",
        lastName1: "Майклсон",
        age1: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name1: "Клаус",
        lastName1: "Майклсон",
        age1: 1093,
        gender: "man",
        status: "vampire"
    }
    ];

    const charactersShortInfo = characters.map(({name1, lastName1, age1}) => ({name1, lastName1, age1}));

//     for (let i = 0; i < characters.length; i++) {
//     let {name1, lastName1, age1} = characters[i];
//     charactersShortInfo.push({name1, lastName1, age1});
// }
    console.group('Second task');
    console.log(charactersShortInfo);
    console.groupEnd();

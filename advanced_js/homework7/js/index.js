
/**
 * Технические требования:
 * При открытии страницы, необходимо получить с сервера список всех пользователей и общий список публикаций.
 * Для этого нужно отправить GET запрос на следующие два адреса:
 * https://ajax.test-danit.com/api/json/users
 * https://ajax.test-danit.com/api/json/posts
 *
 * После загрузки всех пользователей и их публикаций, необоходимо отобразить все публикации на странице.
 * Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), и включать
 * заголовок, текст, а также имя, фамилию и имейл пользователя, который ее разместил.
 *
 * На каждой карточке должна присутствовать иконка или кнопка, которая позволит удалить данную карточку со страницы.
 * При клике на нее необходимо отправить DELETE запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}.
 * После получения подтверждения с сервера (запрос прошел успешно), карточку можно удалить со страницы,
 * используя JavaScript.
 *
 * Более детальную информацию по использованию каждого из этих указанных выше API можно найти здесь.
 * Данный сервер является тестовым. После перезагрузки страницы все изменения, которые отправлялись на сервер,
 * не будут там сохранены. Это нормально, все так и должно работать.
 *
 * Карточки обязательно должны быть реализованы в виде ES6 классов. Для этого необходимо создать класс Card.
 * При необходимости, вы можете добавлять также другие классы.
 *
 * Пока с сервера при открытии страницы загружается информация, показывать анимацию загрузки.
 * Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.
 *
 * Добавить вверху страницы кнопку Добавить публикацию. При нажатии на кнопку, открывать модальное окно,
 * в котором пользователь сможет ввести заголовок и текст публикации. После создания публикации данные
 * о ней необходимо отправить в POST запросе по адресу: https://ajax.test-danit.com/api/json/posts.
 * Новая публикация должна быть добавлена вверху страницы (сортировка в обратном хронологическом порядке).
 * В качестве автора можно присвоить публикации пользователя с id: 1.
 *
 * Добавить функционал (иконку) для редактирования содержимого карточки. После редактирования карточки для
 * подтверждения изменений необходимо отправить PUT запрос по адресу
 * https://ajax.test-danit.com/api/json/posts/${postId}.
 *
 */

import {APIService} from "./classes/APIService.js";
import {Post} from "./classes/Post.js";
import {ModalAdd} from "./classes/ModalAdd.js";
import {ModalEdit} from "./classes/ModalEdit.js";

export let users, posts, modalEdit;

window.onload = async () => {
    const modalAdd = new ModalAdd('form', 'modal', document.body);
    modalEdit = new ModalEdit('form', 'modal', document.body);

    document.querySelector('.newsline__add-button').addEventListener('click', () => {
        modalAdd.build();
    });

    const promiseUsers = new APIService('https://ajax.test-danit.com/api/json/users');
    const promisePosts = new APIService('https://ajax.test-danit.com/api/json/posts');

    users = promiseUsers.sendRequest(true, true);
    posts = promisePosts.sendRequest(true, true);

    users = await users;
    posts = await posts;

    document.querySelector('.lds-ring').remove();

    await users.forEach(user => {
        posts.forEach(post => {
            if (user.id === post.userId) {
                const posts = new Post('div', 'newsline__post', document.querySelector('.newsline__container'),
                    post.id, post.userId, user.name, user.email, post.title, post.body);
                posts.build();
            }
        });
    });
}





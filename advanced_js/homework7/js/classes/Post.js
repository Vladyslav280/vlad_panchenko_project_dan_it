import {Component} from "./Component.js";
import {Container} from "./Container.js";
import {Paragraph} from "./Paragraph.js";
import {Button} from "./Button.js";
import {APIService} from "./APIService.js";
import {modalEdit} from "../index.js"

export class Post extends Component {
    constructor(tagName, className, parentName, id, userId, name, email, title, text) {
        super(tagName, className, parentName);
        this._id = id;
        this._userId = userId;
        this._name = name;
        this._email = email;
        this._title = title;
        this._text = text;
    }
    build() {
        this._component = document.createElement(`${this.tagName}`);
        this._component.classList = `${this.className}`;
        this.parentName.prepend(this._component);

        this._component.setAttribute('idpost', this.id);
        this._component.setAttribute('userId', this.userId);

        this.infobox = new Container('div', 'newsline__info-box', this.component);
        this.infobox.build();
        this.nameParag = new Paragraph('p', 'newsline__name', this.infobox.component, `${this.name}`, 'name');
        this.nameParag.build();
        this.emailParag = new Paragraph('p', 'newsline__email', this.infobox.component, `${this.email}`, 'email');
        this.emailParag.build();
        this.titleParag = new Paragraph('p', 'newsline__title', this.infobox.component, `${this.title}`, 'title');
        this.titleParag.build();
        this.textParag = new Paragraph('p', 'newsline__text', this.infobox.component, `${this.text}`, 'text');
        this.textParag.build();

        this.buttonContainer = new Container('div', 'newsline__button-container', this.component);
        this.buttonContainer.build();
        this.editButton = new Button('button', '', this.buttonContainer.component, 'Edit',
            'click', () => {
                modalEdit.build(this.id, this.userId, this.name, this.email, this.title, this.text);
            });
        this.editButton.build();
        this.deleteButton = new Button('button', '', this.buttonContainer.component, 'Delete',
            'click', () => {
                this.deletePost = new APIService(`https://ajax.test-danit.com/api/json/posts/${this.id}`, 'DELETE', this);

                console.log(this);

                this.deletePost.sendRequest(true, true);

                this.removeComponent();
            });
        this.deleteButton.build();
    }

    get id() {
        return this._id;
    }

    get userId() {
        return this._userId;
    }
    get name() {
        return this._name;
    }
    get email() {
        return this._email;
    }
    get title() {
        return this._title;
    }
    get text() {
        return this._text;
    }
}
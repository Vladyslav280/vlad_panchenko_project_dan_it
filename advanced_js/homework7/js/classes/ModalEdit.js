import {Modal} from "./Modal.js";
import {Input} from "./Input.js";
import {Paragraph} from "./Paragraph.js";
import {Textarea} from "./Textarea.js";
import {APIService} from "./APIService.js";
import {Button} from "./Button.js";
import {users} from "../index.js";

export class ModalEdit extends Modal {
    constructor(tagName, className, parentName) {
        super(tagName, className, parentName);
    }
    build(postId, userId, name, email, title, text) {
        super.build();

        this.component.addEventListener('submit', async event => {
            await event.preventDefault();
            try {
                this.promisePost = new APIService(`https://ajax.test-danit.com/api/json/users/${userId}`, 'PUT',
                    JSON.stringify({
                        name: this.nameInput.value,
                        email: this.emailInput.value,
                    }));

                this.promiseUser = new APIService(`https://ajax.test-danit.com/api/json/posts/${postId}`, 'PUT',
                    JSON.stringify({
                        userId: users[users.length - 1].id,
                        title: this.titleInput.value,
                        body: this.textInput.value,
                    }));

                this.addedPost = this.promisePost.sendRequest(true, true);
                this.addedUser = this.promiseUser.sendRequest(true, true);

                this.target = document.querySelector(`.newsline__post[idpost='${postId}']`);

                this.target.children[0].children[0].innerText = this.nameInput.value;
                this.target.children[0].children[1].innerText = this.emailInput.value;
                this.target.children[0].children[2].innerText = this.titleInput.value;
                this.target.children[0].children[3].innerText = this.textInput.value;

                this.removeComponent();
            } catch (err) {
                console.error(err);
            }
        })

        this.nameInputTitle = new Paragraph('p', 'modal__input', this.component, 'Edit name');
        this.nameInputTitle.build();
        this.nameInput = new Input('input', '', this.nameInputTitle.component, 'text', name, 'name');
        this.nameInput.build();
        this.emailInputTitle = new Paragraph('p', 'modal__input', this.component, 'Edit email');
        this.emailInputTitle.build();
        this.emailInput = new Input('input', '', this.emailInputTitle.component, 'email', email, 'email');
        this.emailInput.build();
        this.titleInputTitle = new Paragraph('p', 'modal__input', this.component, 'Edit title');
        this.titleInputTitle.build();
        this.titleInput = new Textarea('textarea', '', this.titleInputTitle.component, 10, 2, title, 'title');
        this.titleInput.build();
        this.textInputTitle = new Paragraph('p', 'modal__input', this.component, 'Edit text');
        this.textInputTitle.build();
        this.textInput = new Textarea('textarea', '', this.textInputTitle.component, 10, 10, text, 'title');
        this.textInput.build();
        this.buttonSend = new Button('button', 'modal__button', this.component, 'Send');
        this.buttonSend.build();
    }
}
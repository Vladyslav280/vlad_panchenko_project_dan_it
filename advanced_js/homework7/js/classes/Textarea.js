import {Component} from "./Component.js";

export class Textarea extends Component {
    constructor(tagName, className, parentName, cols, rows, text, title) {
        super(tagName, className, parentName);
        this._cols = cols;
        this._rows = rows;
        this._text = text;
        this._title = title
    }
    build() {
        super.build();
        this._component.setAttribute('cols', this._cols);
        this._component.setAttribute('rows', this._rows);
        if (this._text !== '') this._component.value = this._text;
        if (this._title !== '') this._component.setAttribute('title', this._title);
    }
    get title() {
        return this._title;
    }
    get value() {
        return this._component.value;
    }
    set value(val) {
        this._component.value = val
    }
}
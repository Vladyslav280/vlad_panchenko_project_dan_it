import {Component} from "./Component.js";

export class Container extends Component {
    constructor(tagName, className, parentName) {
        super(tagName, className, parentName);
    }
}
import {Component} from "./Component.js";

export class Modal extends Component {
    constructor(tagName, className, parentName) {
        super(tagName, className, parentName);
    }
    build() {
        this._component = document.createElement(`${this.tagName}`);
        if (this.className !== '')this._component.classList = `${this.className}`;
        this.parentName.prepend(this._component);
        this._component.insertAdjacentHTML('beforebegin', `<div class="overlay"></div>`);
        this._component.addEventListener('keydown', event => {
            if (event.key === 'Escape') {
                this.component.remove();
                document.body.querySelector('.overlay').remove();
            }
        });
    }
    removeComponent() {
        super.removeComponent();
        document.body.querySelector('.overlay').remove();
    }
}
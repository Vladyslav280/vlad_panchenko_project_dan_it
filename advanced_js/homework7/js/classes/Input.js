import {Component} from "./Component.js";

export class Input extends Component {
    constructor(tagName, className, parentName, type, value, title) {
        super(tagName, className, parentName);
        this._type = type;
        this._value = value;
        this._title = title;
    }
    build() {
        super.build();
        this.component.setAttribute('type', this.type);
        if (this._value !== '') this._component.value = this._value
        if (this._title !== '') this._component.setAttribute('title', this.title);
    }
    get type() {
        return this._type;
    }
    get value() {
        return this._component.value;
    }
    set value(val) {
        this._component.value = val;
    }
    get title() {
        return this._value;
    }
}
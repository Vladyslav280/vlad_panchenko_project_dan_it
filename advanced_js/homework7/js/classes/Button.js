import {Component} from "./Component.js";

export class Button extends Component {
    constructor(tagName, className, parentName, text, eventName, listener) {
        super(tagName, className, parentName);
        this.text = text;
        this.eventName = eventName;
        this.listener = listener
    }
    build() {
        super.build();
        this.component.innerText = `${this.text}`
        this.component.addEventListener(this.eventName, this.listener)
    }
}
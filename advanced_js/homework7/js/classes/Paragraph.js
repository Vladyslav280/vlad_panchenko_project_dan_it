import {Component} from "./Component.js";

export class Paragraph extends Component {
    constructor(tagName, className, parentName, componentText, title) {
        super(tagName, className, parentName);
        this.componentText = componentText;
        this._title = title;
    }
    build() {
        this._component = document.createElement(`${this.tagName}`);
        this._component.setAttribute('title', this._title);
        this._component.classList = `${this.className}`;
        this._component.innerText = `${this.componentText}`;
        this.parentName.append(this._component);
    }
    get title() {
        return this._title;
    }
}
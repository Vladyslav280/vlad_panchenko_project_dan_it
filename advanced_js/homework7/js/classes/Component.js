export class Component {
    constructor(tagName, className, parentName) {
        this.tagName = tagName;
        this.className = className;
        this.parentName = parentName;
    }
    build() {
        this._component = document.createElement(`${this.tagName}`);
        if (this.className !== '')this._component.classList = `${this.className}`;
        this.parentName.append(this._component);
    }
    removeComponent() {
        this.component.remove();
    }
    get component() {
        return this._component;
    }
}
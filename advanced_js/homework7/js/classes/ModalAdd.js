import {Modal} from "./Modal.js";
import {Paragraph} from "./Paragraph.js";
import {Textarea} from "./Textarea.js";
import {Button} from "./Button.js";
import {APIService} from "./APIService.js";
import {Post} from "./Post.js";
import {users} from "../index.js";

export class ModalAdd extends Modal {
    constructor(tagName, className, parentName) {
        super(tagName, className, parentName);
    }
    build() {
        super.build();

        this.title = new Paragraph('p', 'modal__input', this.component, 'Enter the title');
        this.title.build();
        this.titlearea = new Textarea('textarea', '', this.title.component, 10, 2, '');
        this.titlearea.build();
        this.text = new Paragraph('p', 'modal__input', this.component, 'Enter the text');
        this.text.build();
        this.textarea = new Textarea('textarea', '', this.text.component, 30, 10, '');
        this.textarea.build();
        this.buttonSend = new Button('button', 'modal__button', this.component, 'Send');
        this.buttonSend.build();

        this.component.addEventListener('submit', async event => {
            await event.preventDefault();
            try {
                if (this.titlearea.value === '' || this.textarea.value === '') {
                    throw new Error('Ошибка! Поля не заполнены');
                } else {
                    this.promisePost = new APIService('https://ajax.test-danit.com/api/json/posts', 'POST',
                        JSON.stringify({
                            userId: users[users.length - 1].id,
                            title: this.titlearea.value,
                            body: this.textarea.value
                        }));

                    this.addedPost = this.promisePost.sendRequest(true, true);
                    this.addedPost = await this.addedPost;

                    this.post = new Post('div', 'newsline__post', document.querySelector('.newsline__container'),this.addedPost.id, users[users.length - 1].id, users[users.length  - 1].name, users[users.length - 1].email, this.titlearea.value, this.textarea.value);
                    this.post.build();

                    this.removeComponent();
                }
            } catch (err) {
                console.error(err);
            }
        })
    }
}
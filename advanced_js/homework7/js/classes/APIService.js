export class APIService {
    constructor(url, method = 'GET', body) {
        this.url = url;
        this.method = method;
        this.body = body;
    }
    async sendRequest(showStatus = false, showResult = false) {
        const response = await fetch(this.url, {
            method: this.method,
            body: this.body
        });
        if (this.method === 'DELETE') {
            if (showStatus) console.log(`Status: ${response.status}`);
            if (showResult) console.log(response);
            return true;
        }
        const result = await response.json();
        if (showStatus) console.log(`Status: ${response.status}`);
        if (showResult) console.log(result);
        return result;
    }
}
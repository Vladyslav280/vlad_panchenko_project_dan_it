import React, { useState } from "react";
import PropTypes from 'prop-types';
import './CatalogCard.scss';
import favourite from './favourite.svg';
import favouriteActive from './favouriteActive.svg';
import Modal from '../Modal/Modal';
import { Link } from 'react-router-dom'

function CatalogCard({ id, name, price, picture, code, color, addToBag, toggleFav, favItems, bagItems }) {
     const [active, setActive] = useState(false);

     const changeActive = () => {
          setActive(!active);
     }

     const itemBag = bagItems.filter(el => el.id === id);
     const itemFav = favItems.filter(el => el.id === id);

     return (
          <div className="CatalogCard">
               <div className="CatalogCard__picture">
                    <img src={picture} alt={name} />
               </div>
               <p className="CatalogCard__name">{name}</p>
               <p className="CatalogCard__price">{price}$</p>
               <p className="CatalogCard__color">
                    <span style={{
                         background: `${color}`
                    }} className="CatalogCard__colorCycle"></span>
                    <span className="CatalogCard__colorSpan">{color}</span>

               </p>

               <div className="CatalogCard__footer">
                    <p className="CatalogCard__code">code: {code}</p>
                    <button className={'CatalogCard__favButton'}>{
                         itemFav.length > 0 ?
                              <img src={favouriteActive} alt="favourite active" data-id={id} onClick={toggleFav} /> :
                              <img src={favourite} alt="favourite" data-id={id} onClick={toggleFav} />
                    }</button>
                    {
                         itemBag.length > 0 ?
                              <Link to='../bag' className="CatalogCard__link">
                                   <button className="CatalogCard__ctaButton CatalogCard__ctaButton--added">Go to bag</button>
                              </Link> :
                              <button className="CatalogCard__ctaButton" onClick={changeActive}>Buy</button>
                    }
               </div>
               {
                    active && <Modal
                         active={active}
                         changeActive={changeActive}
                         headerText={`Do you want buy ${name}?`}
                         contentText={
                              `Do you want to add ${name} for ${price}$ to your shopping bag?
                              Click "ok" to continue.`
                         }
                         id={id}
                         transferredFunc={addToBag} />
               }

          </div>
     )
}

CatalogCard.propTypes = {
     id: PropTypes.number,
     name: PropTypes.string,
     price: PropTypes.number,
     picture: PropTypes.string,
     code: PropTypes.number,
     color: PropTypes.string,
     addToBag: PropTypes.func,
     toggleFav: PropTypes.func,
     bagItems: PropTypes.array,
     favItems: PropTypes.array
}

CatalogCard.defaultProps = {
     name: "Untitled product",
     price: 0,
     picture: "./data/pictures/no-image.jpg",
     code: 9999,
     color: "none"
}

export default CatalogCard;
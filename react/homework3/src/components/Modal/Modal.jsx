import React from "react";
import './Modal.scss';
import PropTypes from 'prop-types';

function Modal({ active, transferredFunc, changeActive, headerText, contentText, id }) {

     return (
          <div className={active ? "modal-overlay modal--active" : 'modal-overlay'}
               onClick={(event) => {
                    if (event.target.className === 'modal-overlay modal--active') changeActive();
               }}>
               <div className="modal">
                    <div className="modal__header">
                         <p className="modal__header-text">{headerText}</p>
                    </div>
                    <p className="modal__text">{contentText}</p>
                    <div
                         className="modal__btn-container">
                         <button className="modal__btn" onClick={changeActive} onMouseUp={transferredFunc} data-id={id}>Ok</button>
                         <button className="modal__btn" onClick={changeActive}>Cancel</button>
                    </div>
               </div>
          </div>
     )
}


Modal.propTypes = {
     active: PropTypes.bool,
     transferredFunc: PropTypes.func,
     changeActive: PropTypes.func,
     headerText: PropTypes.string,
     contentText: PropTypes.string,
     id: PropTypes.number
}

export default Modal;
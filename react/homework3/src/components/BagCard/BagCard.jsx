import React, { useState } from "react";
import './BagCard.scss';
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";

function BagCard({ removeFromBag, id, name, price, picture, color, code }) {
     const [active, setActive] = useState(false);

     const changeActive = () => {
          setActive(!active);
     }

     return (
          <div className='BagCard'>
               <div className="BagCard__picture">
                    <img src={picture} alt={name} />
               </div>
               <div className='BagCard__infoContainer'>
                    <p className="BagCard__name">{name}</p>
                    <p className="BagCard__price">{price}$</p>
                    <p className="BagCard__color">
                         <span style={{
                              background: `${color}`
                         }} className="BagCard__colorCycle"></span>
                         <span className="BagCard__colorSpan">{color}</span>
                    </p>
                    <p className='BagCard__code'>code: {code}</p>
               </div>
               <button className='BagCard__removeBtn' data-id={id} onClick={changeActive}>Delete</button>
               {
                    active && <Modal
                         active={active}
                         changeActive={changeActive}
                         transferredFunc={removeFromBag}
                         id={id}
                         headerText={'Do you want remove this item from bag?'}
                         contentText={
                              `Do you want to remove ${name} for ${price}$ from your shopping bag?
                              Click "ok" to continue.`
                         }
                    />
               }
          </div>
     )
}

BagCard.propTypes = {
     removeFromBag: PropTypes.func,
     id: PropTypes.number,
     name: PropTypes.string,
     price: PropTypes.number,
     picture: PropTypes.string,
     color: PropTypes.string,
     code: PropTypes.number
}

export default BagCard;
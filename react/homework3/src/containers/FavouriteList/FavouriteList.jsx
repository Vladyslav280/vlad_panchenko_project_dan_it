import React from "react";
import PropTypes from 'prop-types';
import './FavouriteList.scss';
import CatalogCard from "../../components/CatalogCard/CatalogCard";

function FavouriteList({ addToFav, toggleFav, bagItems, favItems }) {
     return (
          <ul className="favouriteList">
               {
                    favItems.map(el => {
                         const { id, name, price, picture, code, color } = el;

                         return (
                              <li key={id} className="favouriteList__item">
                              <CatalogCard
                                   id={id}
                                   name={name}
                                   price={price}
                                   picture={picture}
                                   code={code}
                                   color={color}
                                   toggleFav={toggleFav}
                                   addToFav={addToFav}
                                   favItems={favItems}
                                   bagItems={bagItems}
                              />
                         </li> 
                         )
                    })
               }
          </ul>
     )
}

FavouriteList.propTypes = {
     addToFav: PropTypes.func,
     toggleFav: PropTypes.func,
     favItems: PropTypes.array,
     bagItems: PropTypes.array
}

export default FavouriteList;
import React from "react";
import PropTypes from 'prop-types';
import './CatalogList.scss';
import CatalogCard from "../../components/CatalogCard/CatalogCard";

function CatalogList({ addToBag, toggleFav, favItems, bagItems, data }) {

     return (
          <ul className="CatalogList">
               {
                    data.map(el => {
                         const { id, name, price, picture, code, color } = el;

                         return (
                              <li key={id} className="CatalogList__item">
                                   <CatalogCard
                                        id={id}
                                        name={name}
                                        price={price}
                                        picture={picture}
                                        code={code}
                                        color={color}
                                        addToBag={addToBag}
                                        toggleFav={toggleFav}
                                        favItems={favItems}
                                        bagItems={bagItems}
                                   />
                              </li>
                         )
                    })
               }
          </ul>
     )
}

CatalogList.propTypes = {
     addToBag: PropTypes.func,
     toggleFav: PropTypes.func,
     favItems: PropTypes.array,
     bagItems: PropTypes.array,
     data: PropTypes.array
}

export default CatalogList;
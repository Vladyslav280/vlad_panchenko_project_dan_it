import './Bag.scss';
import React from 'react';
import BagList from '../../containers/BagList/BagList';
import PropTypes from 'prop-types';

function Bag({ bagItems, removeFromBag, auth, changeAuthentication }) {
     return (
          <>
               {
                    auth ?
                         <BagList
                              bagItems={bagItems}
                              removeFromBag={removeFromBag}
                         />
                         : <div className='bag'>
                              <p className='bag__alertText'>Please, login to continue</p>
                              <button className='bag__loginBtn' onClick={changeAuthentication}>Login</button>
                         </div>
               }
          </>
     )
}

Bag.propTypes = {
     removeFromBag: PropTypes.func,
     bagItems: PropTypes.array,
     auth: PropTypes.bool,
     changeAuthentication: PropTypes.func
}

export default Bag;
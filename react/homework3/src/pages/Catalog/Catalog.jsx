import './Catalog.scss';
import React from 'react';
import CatalogList from '../../containers/CatalogList/CatalogList';
import PropTypes from 'prop-types';

function Catalog({ addToBag, toggleFav, favItems, bagItems, data }) {
     return (
          <CatalogList
               addToBag={addToBag}
               toggleFav={toggleFav}
               bagItems={bagItems}
               favItems={favItems}
               data={data}
          />
     )
}

Catalog.propTypes = {
     addToBag: PropTypes.func,
     toggleFav: PropTypes.func,
     favItems: PropTypes.array,
     bagItems: PropTypes.array,
     data: PropTypes.array
}

export default Catalog;
import './Favourite.scss';
import React from 'react';
import PropTypes from 'prop-types';
import FavouriteList from '../../containers/FavouriteList/FavouriteList';

function Favourite({ favItems, bagItems, addToBag, toggleFav }) {
     return (
          <FavouriteList
               favItems={favItems}
               bagItems={bagItems}
               addToBag={addToBag}
               toggleFav={toggleFav}
          />
     )
}

Favourite.propTypes = {
     favItems: PropTypes.array,
     bagItems: PropTypes.array,
     toggleFav: PropTypes.func,
     addToBag: PropTypes.func

}

export default Favourite;
import { Component } from "react";
import Button from '../Button/Button';
import img from './cross.png';
import './Modal.scss';

class Modal extends Component {
     render() {
          const { active, changeActive, modalData, modalStyles } = this.props
          return (
               <div
               className={active ? "modal-overlay modal--active" : 'modal-overlay'}
               onClick={(event) => {
                    if (event.target.className === 'modal-overlay modal--active') changeActive();
               }}> 
                    <div
                    className="modal"
                    style={{ backgroundColor: modalStyles.background }}>
                         <div
                         className="modal__header"
                         style={{ backgroundColor: modalStyles.headerColor }}>
                              <p
                              className="modal__header-text"
                              style={{ color: modalStyles.headerTextColor }}>
                                   {modalData.title}
                              </p>
                              <p  
                              className="modal__header-btn"
                              onClick={changeActive}
                              style={modalData.closeButton ? { display: "flex" } : { display: "none" }}>
                                   <img
                                   src={img} 
                                   alt="cross"
                                   />
                              </p>
                         </div>
                         <p
                         className="modal__text"
                         style={{ color: modalStyles.textColor}}>
                              {modalData.text}
                         </p>
                         <div
                         className="modal__btn-container">
                              <Button
                                   backgroundcolor={modalStyles.buttonBackground}
                                   className="modal__btn"
                                   text={"Ok"}
                                   onClick={modalData.actions.ok} 
                              />
                              <Button
                                   backgroundcolor={modalStyles.buttonBackground}
                                   className="modal__btn"
                                   text={"Cancel"}
                                   onClick={modalData.actions.cancel}
                              />
                         </div>
                    </div>
               </div>
          )
     }
}

export default Modal;
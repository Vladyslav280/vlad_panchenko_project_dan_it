import { Component } from 'react';
import './Button.scss'

class Button extends Component {

     render() {
          const {text, className, backgroundcolor, onClick, id} = this.props;
          return (
               <button 
                       onClick={onClick}
                       className={className}
                       style={{
                            background: backgroundcolor
                       }}
                       id={id}
               >{text}
               </button>
          )
     }
}

export default Button;
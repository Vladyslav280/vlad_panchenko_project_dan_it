import {
     ADD_TO_BAG,
     REMOVE_FROM_BAG
} from "./types";

const initialState = {
     bagItems: []
};

const bag = (state = initialState, action) => {
     switch (action.type) {
          case ADD_TO_BAG: {
               const selectedItemID = action.payload;
               const products = action.extra;
               let newItem;
               products.map(el => {
                    if (el.id === parseInt(selectedItemID)) newItem = el
               });
               const addToBag = [...state.bagItems, newItem];
               return {
                    ...state,
                    bagItems: addToBag
               }
          }
          case REMOVE_FROM_BAG: {
               const removedItemID = action.payload;
               const products = action.extra;
               let removedItem;
               products.map(el => {
                    if (el.id === parseInt(removedItemID)) removedItem = el;
               })
               const removeFromBag = state.bagItems.filter(el => el.id !== removedItem.id);
               return {
                    ...state,
                    bagItems: removeFromBag
               }
          }
          default: {
               return state;
          }
     }
}

export default bag;
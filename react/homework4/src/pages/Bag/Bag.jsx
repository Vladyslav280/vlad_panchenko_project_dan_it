import './Bag.scss';
import React from 'react';
import BagCard from '../../components/BagCard/BagCard'
import Loading from '../../components/Loading/Loading';
import { useDispatch, useSelector } from 'react-redux';
import { changeAuthentication } from '../../store/authentication/actions';
import { removeFromBag } from '../../store/bag/actions';

function Bag() {
     const dispatch = useDispatch();
     const changeAuthenticationAction = () => dispatch(changeAuthentication());
     const removeFromBagAction = (event, data) => dispatch(removeFromBag(event, data));

     const authentication = useSelector(state => state.authentication);
     const bag = useSelector(state => state.bag);
     const products = useSelector(state => state.products);

     return (
          <>
               {
                    products.isLoading ?
                         <Loading /> :
                         authentication.authentication ?
                              <ul className='bag__list'>
                                   {
                                        bag.bagItems.map(el => {
                                             const { id, name, price, picture, color, code } = el;

                                             return (
                                                  <li key={id}>
                                                       <BagCard
                                                            removeFromBagAction={removeFromBagAction}
                                                            id={id}
                                                            name={name}
                                                            price={price}
                                                            picture={picture}
                                                            color={color}
                                                            code={code}
                                                       />
                                                  </li>
                                             )
                                        })
                                   }
                              </ul>
                              : <div className='bag__emptyPage'>
                                   <p className='bag__alertText'>Please, login to continue</p>
                                   <button className='bag__loginBtn' onClick={changeAuthenticationAction}>Login</button>
                              </div>
               }
          </>
     )
}

export default Bag;
const getData = () => {
  const data = fetch('./data/products.json')
    .then(result => result.json())
    .then(data => {
      return data;
    })
    return data;
}

export default getData;
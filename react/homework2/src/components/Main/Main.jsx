import './Main.scss';
import { Component } from 'react';
import ProductList from '../ProductList/ProductList';
import PropTypes from 'prop-types';

class Main extends Component {
     render() {
          const { addToBag, addToFav, favItems } = this.props;
          return (
               <>
                    <main className="main">
                         <ProductList addToBag={addToBag} addToFav={addToFav} favItems={favItems}/>
                    </main>
               </>
          )
     }
}

Main.propTypes = {
     addToBag: PropTypes.func,
     addToFav: PropTypes.func,
     favItems: PropTypes.array
}

export default Main;
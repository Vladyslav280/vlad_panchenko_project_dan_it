import { Component } from "react";
import './Modal.scss';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

class Modal extends Component {
     render() {
          const { active, addToBag, changeActive, item } = this.props;
          const { name, price } = this.props.item;
          return (
               <div className={active ? "modal-overlay modal--active" : 'modal-overlay'}
                    onClick={(event) => {
                         if (event.target.className === 'modal-overlay modal--active') changeActive();
                    }}>
                    <div className="modal">
                         <div className="modal__header">
                              <p className="modal__header-text">
                                   Do you want buy {name}?
                              </p>
                         </div>
                         <p className="modal__text">
                              Do you want to add {name} for {price}$ to your shopping bag?
                              Click "ok" to continue.
                         </p>
                         <div
                              className="modal__btn-container">
                              <Button
                                   className={'modal__btn'}
                                   funcOnClick={
                                        () => {
                                             addToBag(item)
                                             changeActive();
                                        }
                                   }
                                   innerValue={'Ok'} />
                              <Button
                                   className={'modal__btn'}
                                   funcOnClick={changeActive}
                                   innerValue={'Cancel'} />
                         </div>
                    </div>
               </div>
          )
     }
}

Modal.propTypes = {
     active: PropTypes.bool,
     addToBag: PropTypes.func,
     changeActive: PropTypes.func,
     item: PropTypes.object
}

export default Modal;
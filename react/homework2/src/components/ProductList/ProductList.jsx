import { Component } from "react";
import PropTypes from 'prop-types';
import './ProductList.scss';
import ProductCard from "../ProductCard/ProductCard";
import getData from '../../api/getData';

class ProductList extends Component {
     constructor() {
          super();
          this.state = {
               data: []
          }
     }

     async componentDidMount() {
          let data = await getData();
          await this.setState({
               data: data
          })
     }

     render() {
          const { data } = this.state;
          const { addToBag, addToFav, favItems } = this.props;

          return (
               <ul className="productList">
                    {
                         data.map(item => {
                              const { id, name, price, picture, code, color } = item;
                              
                              return (
                                   <li key={id} className="productList__item">
                                        <ProductCard
                                             id={id}
                                             name={name}
                                             price={price}
                                             picture={picture}
                                             code={code}
                                             color={color}
                                             addToBag={addToBag}
                                             addToFav={addToFav}
                                             favItems={favItems}
                                             item={item} />
                                   </li>
                              )
                         })
                    }
               </ul>
          )
     }
}

ProductList.propTypes = {
     addToBag: PropTypes.func,
     addToFav: PropTypes.func,
     favItems: PropTypes.array
}

export default ProductList;
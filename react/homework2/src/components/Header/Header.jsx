import { Component } from "react";
import './Header.scss';
import shoppingBag from './shopping-bag-icon.png';
import favourite from './favourite.svg';
import PropTypes from 'prop-types';

class Header extends Component {

     render() {
          const { favItems, bagItems } = this.props;
          return (
               <header className='header'>
                    <div className="header__fixed">
                         <div className='header__box'>
                              <div className="header__bagBox">
                                   <div className='header__bag'>
                                        <img src={shoppingBag} alt="shopping bag" />
                                   </div>
                                   <span className="header__span">({bagItems.length} items)</span>
                              </div>

                              <div className="header__favouriteBox">
                                   <div className='header__favourite'>
                                        <img src={favourite} alt="favourite" />
                                   </div>
                                   <span className="header__span">({favItems.length} items)</span>
                              </div>
                         </div>
                    </div>
               </header>
          )
     }
}

Header.propTypes = {
     bagItems: PropTypes.array,
     favItems: PropTypes.array
}

export default Header;
import { Component } from "react";
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {
     render() {
          const { className, innerValue, funcOnClick} = this.props;
          return (
               <button className={className} onClick={funcOnClick}>
                    {innerValue}
               </button>
          )
     }
}

Button.propTypes = {
     className: PropTypes.string,
     innerValue: PropTypes.any
}

Button.defaultProps = {
     className: 'defaultButton',
     innerValue: 'Button'
}

export default Button;
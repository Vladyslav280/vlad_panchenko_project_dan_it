import { Component } from "react";
import PropTypes from 'prop-types';
import './ProductCard.scss';
import favourite from './favourite.svg';
import favouriteActive from './favouriteActive.svg';
import Modal from '../Modal/Modal';
import Button from "../Button/Button";

class ProductCard extends Component {
     constructor() {
          super();
          this.state = {
               active: false
          }
          this.changeActive = this.changeActive.bind(this);
     }

     changeActive() {
          this.setState({ active: !this.state.active })
     }

     render() {
          const { id, name, price, picture, code, color, item, addToBag, addToFav, favItems } = this.props;
          const { changeActive } = this;
          const { active } = this.state;

          return (
               <div className="productCard">
                    <div className="productCard__picture">
                         <img src={picture} alt={name} />
                    </div>
                    <p className="productCard__name">{name}</p>
                    <p className="productCard__price">{price}$</p>
                    <p className="productCard__color">
                         <span style={{
                              background: `${color}`
                         }} className="productCard__colorCycle"></span>
                         <span className="productCard__colorSpan">{color}</span>

                    </p>

                    <div className="productCard__footer">
                         <p className="productCard__code">code: {code}</p>
                         <Button className={'productCard__favButton'} innerValue={
                              favItems.includes(id.toString()) ?
                                   <img src={favouriteActive} alt="favourite active" data-id={id} onClick={addToFav} /> :
                                   <img src={favourite} alt="favourite" data-id={item.id} onClick={addToFav} />
                         } />
                         <Button className={"productCard__ctaButton"} funcOnClick={this.changeActive} innerValue={'Buy'} />
                    </div>
                    {
                         active && <Modal
                              active={active}
                              changeActive={changeActive}
                              item={item}
                              addToBag={addToBag} />
                    }

               </div>
          )
     }
}

ProductCard.propTypes = {
     id: PropTypes.number,
     name: PropTypes.string,
     price: PropTypes.number,
     picture: PropTypes.string,
     code: PropTypes.number,
     color: PropTypes.string,
     addToBag: PropTypes.func,
     addToFav: PropTypes.func,
     item: PropTypes.object,
     favItems: PropTypes.array
}

ProductCard.defaultProps = {
     name: "Untitled product",
     price: 0,
     picture: "./data/pictures/no-image.jpg",
     code: 9999,
     color: "none"
}


export default ProductCard;
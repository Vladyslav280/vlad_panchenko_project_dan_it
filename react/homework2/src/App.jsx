import { Component } from 'react';
import Header from './components/Header/Header';
import Main from './components/Main/Main';

class App extends Component {
  constructor() {
    super();
    this.state = {
      bagItems: [],
      favItems: []
    }
    this.addToBag = this.addToBag.bind(this);
    this.addToFav = this.addToFav.bind(this);
  }

  componentDidMount() {
    let state = JSON.parse(localStorage.getItem('state'))
    this.setState(state)
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state) !== JSON.stringify(prevState)) {
      localStorage.setItem('state', JSON.stringify(this.state))
    }
  }

  addToBag(item) {
    const addToBag = [...this.state.bagItems, item]
    this.setState({ bagItems: addToBag });
  }

  addToFav(event) {
    const favourite =  event.target.dataset.id;
    const addFav = [...this.state.favItems, favourite];
    const removeFav = this.state.favItems.filter(el => el !== favourite);

    if(this.state.favItems.includes(favourite)) {
      this.setState({ favItems: removeFav });
    } else {
      this.setState({ favItems: addFav });
    }
  }

  

  render() {
    const { bagItems, favItems } = this.state;
    const { addToBag, addToFav } = this;
    return (
      <>
        <Header bagItems={bagItems} favItems={favItems} />
        <Main addToBag={addToBag} addToFav={addToFav} favItems={favItems} />
      </>
    )
  }
}

export default App;

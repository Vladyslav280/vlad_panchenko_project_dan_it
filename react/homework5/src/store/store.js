import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import { devTools, logger } from './utilities';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';

const persistConfig = {
     key: 'root',
     storage: storage,
     whitelist: ['bag', 'favourite']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)


const store = createStore(
     persistedReducer,
     compose(applyMiddleware(logger, thunk), devTools)
);


export const persistor = persistStore(store);

export default store


